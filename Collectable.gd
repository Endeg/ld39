extends Area2D

export var message = "Hey, it's a collectable!"

func _ready():
	get_node("Anim").play("floating")

func _on_EnergyCrystall_body_enter( body ):
	if body extends preload("res://Player.gd"):
		self.queue_free()
		var infoLine = get_tree().get_root().get_node("Game/UI/InformationLine")
		if infoLine != null:
			infoLine.showMessage(message)
		body.collectablesFound += 1
