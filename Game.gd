extends Node

const START_MESSAGE = "Use Arrow keys and Z to control Bot LD39 to recover energy crystal from this planet."
const CONGLATURATIONS = "Conglaturation !!! you have completed a great game and prooved the justice of our culture!"

func _ready():
	get_node("UI/InformationLine").showMessage(START_MESSAGE)
	get_node("UI/GameOver").hide()
	get_node("UI/EndMessage").hide()
	get_node("Player").maxCollectables = get_node("Collectables").get_child_count()

func _on_Flag_body_enter(body):
	if body extends preload("res://Player.gd") and body.canGoHome:
		get_node("UI/InformationLine").showMessage(CONGLATURATIONS)
		body.goHome()
