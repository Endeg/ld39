extends KinematicBody2D

const PLAYER_ACCEL = 2.2
const MAX_PLAYER_SPEED = 2
const PLAYER_JUMP_SPEED = 3.1

const MAX_FALL_SPEED = 5
const FALL_ACCEL = 4

var lblPlayerEnergy = null

var xVel = Vector2(0, 0)
var yVel = Vector2(0, 0)

const MAX_JUMP_TIMER = 0.75

var jumpTimer = 0.75

var particlesLeft = null
var particlesRight = null

const MAX_ENERGY = 550

const JUMP_COST = 1

var energy = MAX_ENERGY

var gameOver = false

var anim = null

var canGoHome = false
var goingHome = false

const GO_HOME_SPEEED = 20

var maxCollectables = 0
var collectablesFound = 0

func _ready():
	set_fixed_process(true)
	
	lblPlayerEnergy = get_node("/root/Game/UI/PlayerEnergy")
	assert lblPlayerEnergy != null
	
	particlesLeft = get_node("ParticlesLeft")
	assert particlesLeft != null
	particlesRight = get_node("ParticlesRight")
	assert particlesRight != null
	anim = get_node("Anim")
	assert anim != null
	
	anim.play("stay")

func _fixed_process(delta):
	if not goingHome:
		if not get_node("StayCheckRay").is_colliding():
			yVel.y += FALL_ACCEL * delta
			if yVel.y > MAX_FALL_SPEED:
				yVel.y = MAX_FALL_SPEED
		
		var jumpAvailable = false
		jumpTimer += delta
		if jumpTimer > MAX_JUMP_TIMER:
			jumpTimer = MAX_JUMP_TIMER
			jumpAvailable = true
	
		if test_move(yVel):
			yVel.y = 0
	
		if not gameOver:
			if Input.is_action_pressed("PlayerLeft"):
				xVel.x -= PLAYER_ACCEL * delta
				if xVel.x < -MAX_PLAYER_SPEED:
					xVel.x = -MAX_PLAYER_SPEED
				set_scale(Vector2(-1, 1))
				if not (anim.is_playing() and anim.get_animation() == "walk"):
					anim.play("walk")
	
			elif Input.is_action_pressed("PlayerRight"):
				xVel.x += PLAYER_ACCEL * delta
				if xVel.x > MAX_PLAYER_SPEED:
					xVel.x = MAX_PLAYER_SPEED
				set_scale(Vector2(1, 1))
				if not (anim.is_playing() and anim.get_animation() == "walk"):
					anim.play("walk")
	
			else:
				xVel.x = lerp(xVel.x, 0, 0.7)
				anim.play("stay")
	
			if jumpAvailable and Input.is_action_pressed("PlayerJump"):
				yVel.y = -PLAYER_JUMP_SPEED
				jumpTimer = 0
				particlesLeft.set_emitting(true)
				particlesRight.set_emitting(true)
				energy -= JUMP_COST
				get_node("Sound").play("jetpack")
	
			if yVel.y < 0:
				anim.play("inAir")
			elif yVel.y > 0:
				anim.play("landing")
	
		else:
			xVel.x = lerp(xVel.x, 0, 0.7)
			if Input.is_action_pressed("Reset"):
				get_tree().reload_current_scene()
	
		move(xVel)
		move(yVel)
	
		energy -= abs(xVel.x) * delta * 0.001
		
		gameOverCheck()

		lblPlayerEnergy.set_text(makePercent(energy, MAX_ENERGY))
	else:
		var homeRayPos = get_parent().get_node("Flag/HomeRay").get_global_pos()
		var currentPos = get_pos()
		currentPos.y -= GO_HOME_SPEEED * delta
		currentPos.x = lerp(currentPos.x, homeRayPos.x, 0.2)
		set_pos(currentPos)

func goHome():
	goingHome = true
	anim.play("goingHome")
	get_node("AnimationPlayer").play("FloatingHome")
	get_parent().get_node("UI/EndMessage").show()
	var collectablesLabel = get_parent().get_node("UI/Collectables")
	collectablesLabel.set_text("Collectables found: " + var2str(collectablesFound) + "/" + var2str(maxCollectables))
	collectablesLabel.show()

func makePercent(value, maxValue):
	var actual = 0
	if value > 0:
		actual = value
	return var2str(int((actual / maxValue) * 100)) + "%"

func gameOverCheck():
	if energy < 0 and not gameOver:
		energy = 0
		print("Game Over")
		gameOver = true
		get_node("Sound").play("out_of_power")
		anim.play("turnOff")
		var lblGameOver = get_node("/root/Game/UI/GameOver")
		if lblGameOver != null:
			lblGameOver.show()
		var lblInfoLine = get_node("/root/Game/UI/InformationLine")
		if lblInfoLine != null:
			lblInfoLine.showMessage("Bot LD39 was unable to complete it's task. Humanity is doomed.")

func _on_TooHighTrigger_body_enter(body):
	if not goingHome and body == self:
		energy -= MAX_ENERGY * 2
		gameOverCheck()
