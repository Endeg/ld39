extends Node2D

func _ready():
	get_node("Anim").play("floating")

func _on_EnergyCrystall_body_enter(body):
	if body extends preload("res://Player.gd"):
		var infoLine = get_tree().get_root().get_node("Game/UI/InformationLine")
		if infoLine != null:
			infoLine.showMessage("Energy crystall is found! Return to the start pad!")
		self.queue_free()
		body.canGoHome = true
