extends Label

var active = false

func showMessage(message):
	set_size(Vector2(1, 1))
	set_text(message)
	var current = get_pos()
	current.x = 640
	set_pos(current)
	active = true
	get_node("Sound").play("info")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if active:
		var current = get_pos()
		current.x -= delta
		set_pos(current)
		
		if current.x < -get_size().x - 5:
			active = false
			#get_node("Sound").play("info")
